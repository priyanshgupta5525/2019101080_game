import pygame
import time
import random
import config as glob

pygame.init()

clock = pygame.time.Clock()

game_display = pygame.display.set_mode((glob.display_width,glob.display_height))


def level_clear(k):

    score1=str(glob.score_mickey)
    score2 = str(glob.score_super)
    lvl1 =str(glob.level_mickey+1)
    lvl2=str(glob.level_super+1)
    if glob.turn == 1:
        glob.x_mickey = ((glob.display_width/2) - (glob.block_img_width/2))
        glob.y_mickey = (glob.display_height - glob.block_img_width)
        glob.score_mickey +=int(k)*5
        glob.score_mickey +=int(int(k)/2)*5
        glob.score_mickey = glob.score_mickey + glob.score_mickey*4*glob.level_mickey
        glob.score_mickey +=40+40*glob.level_mickey
        score1=str(glob.score_mickey)
        font = set_font(50)
        game_display.fill((0,0,0))
        text_surf,text_rect=text_obj("FINISHED LEVEL: "+lvl1,font , (255,0,0,))
        text_rect.center = ((glob.display_width/2),(glob.display_height/2))
        pygame.draw.rect(game_display, (128,0,128) , ((glob.display_width/2-500),(glob.display_height/2-35),1000,70))
        font = set_font(30)
        score1_surf,score1_rect = text_obj("PLAYER-1 SCORE : "+score1,font , (255,255,255))
        score1_rect.center = ((glob.display_width/2),(glob.display_height/2+70))
        score2_surf,score2_rect = text_obj("PLAYER-2 SCORE : "+score2 ,font ,(255,255,255))
        score2_rect.center = ((glob.display_width/2), (glob.display_height/2+120))
        start_surf,start_rect = text_obj("Starting : Player 2" ,font ,(255,255,255))
        start_rect.center = ((glob.display_width/2) , (glob.display_height-50))
        pygame.draw.rect(game_display , (0,128,0) ,((glob.display_width/2 - 150) ,(glob.display_height-75),300,50))
        glob.turn=2
        game_display.blit(text_surf,text_rect)
        game_display.blit(score1_surf,score1_rect)
        game_display.blit(score2_surf, score2_rect)
        game_display.blit(start_surf , start_rect)
        glob.level_mickey+=1
    else:
        glob.x_super = ((glob.display_width/2) - (glob.block_img_width/2))
        glob.y_super = (glob.display_height - glob.block_img_width)
        glob.level_super+=1
        glob.score_super+=int(k)*5
        glob.score_super+=int(int(k)/2)*5
        glob.score_super = glob.score_super+glob.score_super*glob.level_super*4
        glob.score_super+=40+40*glob.level_super
        score2=str(glob.score_super)
        font = set_font(50)
        game_display.fill((0,0,0))
        text_surf,text_rect=text_obj("FINISHED LEVEL: "+lvl2,font , (255,0,0,))
        text_rect.center = ((glob.display_width/2),(glob.display_height/2))
        pygame.draw.rect(game_display, (128,0,128) , ((glob.display_width/2-500),(glob.display_height/2-35),1000,70))
        font = set_font(30)
        score1_surf,score1_rect = text_obj("PLAYER-1 SCORE : "+score1,font , (255,255,255))
        score1_rect.center = ((glob.display_width/2),(glob.display_height/2+70))
        score2_surf,score2_rect = text_obj("PLAYER-2 SCORE : "+score2 ,font ,(255,255,255))
        score2_rect.center = ((glob.display_width/2), (glob.display_height/2+120))
        start_surf,start_rect = text_obj("Starting : Player 1" ,font ,(255,255,255))
        start_rect.center = ((glob.display_width/2) , (glob.display_height-50))
        pygame.draw.rect(game_display , (0,128,0) ,((glob.display_width/2 - 150) ,(glob.display_height-75),300,50))
        glob.turn = 1
        game_display.blit(text_surf,text_rect)
        game_display.blit(score1_surf,score1_rect)
        game_display.blit(score2_surf, score2_rect)
        game_display.blit(start_surf , start_rect)
    pygame.display.update()
    time.sleep(3)
    glob.start_time = pygame.time.get_ticks()
    main_game()


def menu_page():
    menu_exit = False
    while not menu_exit:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                menu_exit = True
                pygame.quit()
                quit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                x,y = pygame.mouse.get_pos()
                if glob.display_width/2-225<=x<=glob.display_width/2-75 and glob.display_height/2-20<=y<=glob.display_height/2+20:
                    glob.start_time=pygame.time.get_ticks()
                    main_game()
                if glob.display_width/2+25<=x<=glob.display_width/2+175 and glob.display_height/2-20<=y<=glob.display_height/2+20:
                    pygame.quit()
                    quit()
        game_display.fill((255,255,255))
        font = set_font(30)
        start_surf,start_rect = text_obj("START" ,font , (0,255,0))
        start_rect.center = (((glob.display_width/2)-150) , (glob.display_height/2))
        quit_surf,quit_rect = text_obj("QUIT", font , (255,0,0))
        quit_rect.center = (((glob.display_width/2)+100) , (glob.display_height/2))
        pygame.draw.rect(game_display , (0,0,0) , (((glob.display_width/2)-225) , ((glob.display_height/2)-20) , 150 , 40 ))
        pygame.draw.rect(game_display , (0,0,0) , (((glob.display_width/2)+25) , ((glob.display_height/2)-20) , 150 ,40 ))
        game_display.blit(start_surf , start_rect)
        game_display.blit(quit_surf , quit_rect)
        pygame.display.update()
        clock.tick(30)


def crash(k):
    score1=str(glob.score_mickey)
    score2 = str(glob.score_super)
    if glob.turn == 1:
        glob.x_mickey = ((glob.display_width/2) - (glob.block_img_width/2))
        glob.y_mickey = (glob.display_height - glob.block_img_width)
        glob.score_mickey +=int(k)*5
        glob.score_mickey +=int(int(k)/2)*5
        glob.score_mickey = glob.score_mickey + glob.score_mickey*4*glob.level_mickey
        score1=str(glob.score_mickey)
        font = set_font(50)
        game_display.fill((0,0,0))
        text_surf,text_rect=text_obj("PLAYER 1 HAS CRASHED",font , (255,0,0,))
        text_rect.center = ((glob.display_width/2),(glob.display_height/2))
        pygame.draw.rect(game_display, (128,0,128) , ((glob.display_width/2-500),(glob.display_height/2-35),1000,70))
        font = set_font(30)
        score1_surf,score1_rect = text_obj("PLAYER-1 SCORE : "+score1,font , (255,255,255))
        score1_rect.center = ((glob.display_width/2),(glob.display_height/2+70))
        score2_surf,score2_rect = text_obj("PLAYER-2 SCORE : "+score2 ,font ,(255,255,255))
        score2_rect.center = ((glob.display_width/2), (glob.display_height/2+120))
        start_surf,start_rect = text_obj("Starting : Player 2" ,font ,(255,255,255))
        start_rect.center = ((glob.display_width/2) , (glob.display_height-50))
        pygame.draw.rect(game_display , (0,128,0) ,((glob.display_width/2 - 150) ,(glob.display_height-75),300,50))
        glob.turn=2
        game_display.blit(text_surf,text_rect)
        game_display.blit(score1_surf,score1_rect)
        game_display.blit(score2_surf, score2_rect)
        game_display.blit(start_surf , start_rect)
    else:
        glob.x_super = ((glob.display_width/2) - (glob.block_img_width/2))
        glob.y_super = (glob.display_height - glob.block_img_width)

        glob.score_super+=int(k)*5
        glob.score_super+=int(int(k)/2)*5
        glob.score_super = glob.score_super+glob.score_super*glob.level_super*4
        score2=str(glob.score_super)
        font = set_font(50)
        game_display.fill((0,0,0))
        text_surf,text_rect=text_obj("PLAYER 2 HAS CRASHED",font , (255,0,0,))
        text_rect.center = ((glob.display_width/2),(glob.display_height/2))
        pygame.draw.rect(game_display, (128,0,128) , ((glob.display_width/2-500),(glob.display_height/2-35),1000,70))
        font = set_font(30)
        score1_surf,score1_rect = text_obj("PLAYER-1 SCORE : "+score1,font , (255,255,255))
        score1_rect.center = ((glob.display_width/2),(glob.display_height/2+70))
        score2_surf,score2_rect = text_obj("PLAYER-2 SCORE : "+score2 ,font ,(255,255,255))
        score2_rect.center = ((glob.display_width/2), (glob.display_height/2+120))
        start_surf,start_rect = text_obj("Starting : Player 1" ,font ,(255,255,255))
        start_rect.center = ((glob.display_width/2) , (glob.display_height-50))
        pygame.draw.rect(game_display , (0,128,0) ,((glob.display_width/2 - 150) ,(glob.display_height-75),300,50))
        glob.turn = 1
        game_display.blit(text_surf,text_rect)
        game_display.blit(score1_surf,score1_rect)
        game_display.blit(score2_surf, score2_rect)
        game_display.blit(start_surf , start_rect)
    pygame.display.update()
    time.sleep(3)
    glob.start_time = pygame.time.get_ticks()
    main_game()

def text_obj(text , font ,color):
    text_surf  = font.render(text , True , color)
    return text_surf , text_surf.get_rect()

def set_font(font_size):
    font = pygame.font.Font("freesansbold.ttf" , font_size)
    return font


def block_insert(i,x,y):
    game_display.blit(glob.block_img , (x,y))
    glob.pos_hurdle[i]=[x,y]

def super_insert(x,y):
    game_display.blit(glob.super_img , (x,y))

def explo_insert(i,x,y):
    game_display.blit(glob.explo_img , (x,y))
    glob.pos_hurdle[i]= [x,y]

def mickey_insert(x,y):
    game_display.blit(glob.mickey_img , (x,y))

def main_game():


    game_exit=True
    while game_exit:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if glob.turn == 1:
                    glob.x_super_change=0
                    glob.y_super_change=0
                    if event.key==pygame.K_LEFT:
                        glob.x_mickey_change= -5
                        glob.y_mickey_change =0
                    elif event.key == pygame.K_RIGHT:
                        glob.x_mickey_change = 5
                        glob.y_mickey_change =0
                    elif event.key == pygame.K_UP:
                        glob.y_mickey_change = -5
                        glob.x_mickey_page = 0
                    elif event.key==pygame.K_DOWN:
                        glob.y_mickey_change = 5
                        glob.x_mickey_change=0
                    elif event.key == pygame.K_ESCAPE:
                        yo_start=pygame.time.get_ticks()
                        yo = True
                        while yo:
                            for event_yo in pygame.event.get():
                                if event_yo.type == pygame.QUIT:
                                    pygame.quit()
                                    game.quit()
                                if event_yo.type == pygame.KEYDOWN:
                                    if event_yo.key == pygame.K_ESCAPE:
                                        yo =False
                        yo_end = pygame.time.get_ticks()
                        glob.start_time+=yo_end-yo_start
                    else:
                        glob.x_mickey_change=0
                        glob.y_mickey_change=0
                if glob.turn == 2:
                    glob.x_mickey_change=0
                    glob.y_mickey_change=0
                    if event.key==pygame.K_a:
                        glob.x_super_change= -5
                        glob.y_super_change =0
                    elif event.key == pygame.K_d:
                        glob.x_super_change = 5
                        glob.y_super_change =0
                    elif event.key == pygame.K_w:
                        glob.y_super_change = -5
                        glob.x_super_page = 0
                    elif event.key==pygame.K_s:
                        glob.y_super_change=5
                        glob.x_super_change = 0
                    elif event.key == pygame.K_ESCAPE:
                        yo_start=pygame.time.get_ticks()
                        yo = True
                        while yo:
                            for event_yo in pygame.event.get():
                                if event_yo.type == pygame.QUIT:
                                    pygame.quit()
                                    game.quit()
                                if event_yo.type == pygame.KEYDOWN:
                                    if event_yo.key == pygame.K_ESCAPE:
                                        yo =False
                        yo_end=pygame.time.get_ticks()
                        glob.start_time+=yo_end-yo_start

                    else:
                        glob.x_super_change=0
                        glob.y_super_change=0

            if event.type==pygame.KEYUP:
                glob.x_mickey_change=0
                glob.y_mickey_change=0
                glob.x_super_change = 0
                glob.y_super_change=0
        if glob.x_mickey<=0 and glob.x_mickey_change == -5:
            glob.x_mickey_change=0
        if glob.x_mickey >= (glob.display_width-glob.block_img_width) and glob.x_mickey_change == 5:
            glob.x_mickey_change = 0
        if glob.y_mickey <= (glob.display_height - (12*glob.block_img_height)) and glob.y_mickey_change == -5:
            glob.y_mickey_change = 0
        if glob.y_mickey>=glob.display_height-glob.block_img_height and glob.y_mickey_change == 5:
            glob.y_mickey_change=0
        if glob.x_super<=0 and glob.x_super_change == -5:
            glob.x_super_change=0
        if glob.x_super >= (glob.display_width-glob.block_img_width) and glob.x_super_change == 5:
            glob.x_super_change = 0
        if glob.y_super <= (glob.display_height - (12*glob.block_img_height)) and glob.y_super_change == -5:
            glob.y_super_change = 0
        if glob.y_super>=glob.display_height-glob.block_img_height and glob.y_super_change == 5:
            glob.y_super_change=0


        glob.x_mickey+=glob.x_mickey_change
        glob.y_mickey+=glob.y_mickey_change
        glob.x_super+= glob.x_super_change
        glob.y_super+= glob.y_super_change
        if glob.turn==1:
            for i in glob.pos_hurdle:
                if i[0]-glob.block_img_width<=glob.x_mickey<=i[0]+glob.block_img_width and i[1]-glob.block_img_height<=glob.y_mickey<=i[1]+glob.block_img_height:
                    k = ((glob.display_height - glob.y_mickey)/glob.block_img_height)
                    crash(k)

            if glob.y_mickey<=glob.finish:
                k = ((glob.display_height - glob.y_mickey)/glob.block_img_height)
                level_clear(k)
        if glob.turn == 2:
            for i in glob.pos_hurdle:
                if i[0]-glob.block_img_width<=glob.x_super<=i[0]+glob.block_img_width and i[1]-glob.block_img_height<=glob.y_super<=i[1]+glob.block_img_height:
                    k = ((glob.display_height - glob.y_super)/glob.block_img_height)
                    crash(k)
            if glob.y_super<=glob.finish:
                k = ((glob.display_height - glob.y_super)/glob.block_img_height)
                level_clear(k)
        if glob.turn ==1:
            lvl = glob.level_mickey
        else:
            lvl = glob.level_super


        if glob.xr>(glob.display_width-glob.block_img_width):
            glob.xr_change= -6-lvl*4
        if glob.xr<=0:
            glob.xr_change = 6+lvl*4
        if glob.xl>(glob.display_width-glob.block_img_width):
            glob.xl_change = 10+lvl*4
        if glob.xl<0:
            glob.xl_change = -10-lvl*4
        glob.xr+=glob.xr_change
        glob.xl-=glob.xl_change
        ticks= pygame.time.get_ticks()-glob.start_time
        milli=ticks%1000
        sec=int(ticks/1000)
        game_display.fill((0,0,0))
        font= set_font(20)
        watch = str(sec)+"."+str(milli)
        time_surf,time_rect = text_obj(watch,font,(255,255,255))
        time_rect.center = ((glob.display_width-50),13)
        game_display.blit(time_surf,time_rect)
        pygame.draw.rect(game_display , (128,0,0) , (0,(glob.display_height - (2*glob.block_img_height)),glob.display_width , glob.block_img_height))
        pygame.draw.rect(game_display , (128,0,0) , (0,(glob.display_height - (4*glob.block_img_height)),glob.display_width , glob.block_img_height))
        pygame.draw.rect(game_display , (128,0,0) , (0,(glob.display_height - (6*glob.block_img_height)),glob.display_width , glob.block_img_height))
        pygame.draw.rect(game_display , (128,0,0) , (0,(glob.display_height - (8*glob.block_img_height)),glob.display_width , glob.block_img_height))
        pygame.draw.rect(game_display , (128,0,0) , (0,(glob.display_height - (10*glob.block_img_height)),glob.display_width , glob.block_img_height))
        pygame.draw.rect(game_display , (0,128,0) , (0,(glob.display_height - glob.block_img_height), glob.display_width , glob.block_img_height))
        pygame.draw.rect(game_display , (0,128,0) , (0,(glob.display_height - (12*glob.block_img_height)), glob.display_width , glob.block_img_height))
        block_insert(0,100,(glob.display_height-(2*glob.block_img_height)))
        block_insert(1,500,(glob.display_height-(4*glob.block_img_height)))
        block_insert(2,1300,(glob.display_height-(6*glob.block_img_height)))
        block_insert(3,700,(glob.display_height-(8*glob.block_img_height)))
        block_insert(4,800,(glob.display_height-(2*glob.block_img_height)))
        block_insert(5,1100,(glob.display_height-(4*glob.block_img_height)))
        block_insert(6,250,(glob.display_height-(6*glob.block_img_height)))
        block_insert(7,100,(glob.display_height-(8*glob.block_img_height)))
        block_insert(8,400,(glob.display_height-(10*glob.block_img_height)))
        block_insert(9,1200,(glob.display_height-(10*glob.block_img_height)))
        explo_insert(10,(0+glob.xr), (glob.display_height - (3*glob.block_img_height)))
        explo_insert(11,((glob.display_width-glob.block_img_width)-glob.xl), (glob.display_height - (5*glob.block_img_height)))
        explo_insert(12,(0+glob.xr), (glob.display_height - (7*glob.block_img_height)))
        explo_insert(13,((glob.display_width-glob.block_img_width)-glob.xl), (glob.display_height - (9*glob.block_img_height)))
        explo_insert(14,(0+glob.xr), (glob.display_height - (11*glob.block_img_height)))
        font = set_font(30)
        fin_surf,fin_rect = text_obj("FINISH LINE",font,(0,0,0))
        fin_rect.center = (glob.display_width/2,glob.display_height-12*64+32)
        game_display.blit(fin_surf,fin_rect)
        if glob.turn==1:
            mickey_insert(glob.x_mickey,glob.y_mickey )
            font = set_font(20)
            text_surf,text_rect =text_obj("PLAYER-1",font,(0,0,128))
            text_rect.center = (42,13)
            game_display.blit(text_surf,text_rect)
            arr =str(glob.level_mickey+1)
            level_surf,level_rect=text_obj("LEVEL-"+arr,font,(128,255,255))
            level_rect.center = (glob.display_width/2,13)
            game_display.blit(level_surf,level_rect)
        else:
            super_insert(glob.x_super , glob.y_super)
            font = set_font(20)
            text_surf,text_rect =text_obj("PLAYER-2",font,(0,0,128))
            text_rect.center = (42,13)
            game_display.blit(text_surf,text_rect)
            arr =str(glob.level_super+1)
            level_surf,level_rect=text_obj("LEVEL-"+arr,font,(128,255,255))
            level_rect.center = (glob.display_width/2,13)
            game_display.blit(level_surf,level_rect)

        pygame.display.update()
        clock.tick(60)

menu_page()